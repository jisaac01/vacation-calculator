import React, { Component } from 'react';
import DatePicker from 'react-datepicker'

import 'react-datepicker/dist/react-datepicker.css'

class StartDate extends Component {
  render() {
    return <DatePicker
     selected={this.props.parentState.startDate}
     onChange={this.props.handleChange} />;
  }
};

export default StartDate;
