import React, { Component } from 'react';

class CalculatedOutput extends Component {
  render() {
    return (
        <div>
          <div>
            <label>Working Days since then: </label>
            <span>{this.props.parentState.workingDays}</span>
          </div>
          <div>
            <label>Accrual Rate (Hours): </label>
            <span>{this.props.parentState.hourlyRate} /hr, </span>
            <span>{this.props.parentState.dailyRate} /day, </span>
            <span>{this.props.parentState.weeklyRate} /week</span>
          </div>
          <div>
            <label>Hours accrued: </label>
            <span>{this.props.parentState.hoursAccrued}</span>
          </div>
        </div>
    );
  }
}

export default CalculatedOutput;
