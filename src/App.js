import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import StartDate from './StartDate';
import CalculatedOutput from './CalculatedOutput';
import moment from 'moment'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      hoursAccrued: 0,
      rate: 0
    };
  }

  updateRate = (e) => {
    this.setState({rate: e.target.value}
      , () => {
          console.log("Set rate state to: " + this.state.rate.toString());
          this.updateAccrued();
        }
    );
  };

  updateStartDate = (date) => {
    this.setState({startDate: date}
    , () => {
          console.log("Set startDate state to: " + this.state.startDate.toString());
          this.updateAccrued();
      }
    );
  };

  updateAccrued = () => {
    let hoursWorked = this.updateWorkingDays(new Date(this.state.startDate), moment()) * 8;

    let annualWorkingDays = this.getWorkingDays(new Date(2016, 1, 1), new Date(2016, 12, 31));
    let hourlyRate = this.state.rate / annualWorkingDays / 8;
    this.setState({ hourlyRate: hourlyRate.toFixed(2) });
    this.setState({ dailyRate: (hourlyRate * 8).toFixed(2) });
    this.setState({ weeklyRate: (hourlyRate * 40).toFixed(2) });

    let hoursAccrued = (hourlyRate * hoursWorked).toFixed(2);
    this.setState({ hoursAccrued: hoursAccrued })
  }

  updateWorkingDays = (startDate, endDate) => {
    let workingDays = this.getWorkingDays(startDate, endDate);
    this.setState({workingDays: workingDays}
        , () => console.log("Set workingDays to: " + this.state.workingDays.toString())
    );
    return workingDays;
  }

  getWorkingDays = (startDate, endDate) => {
    let result = 0;

    let currentDate = startDate;
    while (currentDate <= endDate)  {

      let weekDay = currentDate.getDay();
      if(weekDay !== 0 && weekDay !== 6)
        result++;

      currentDate.setDate(currentDate.getDate()+1);
    }

    return result;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Vacation Calculator</h2>
        </div>
        <div>
          <h3>Enter your start date, or the last day you had no vacation</h3>
          <label>Start Date</label>
          <StartDate handleChange={this.updateStartDate} parentState={this.state}/>
        </div>
        <div className="AccrualRate">
          <h3>Enter the number of hours of vacation you get per year (40 hrs/week)</h3>
          Accrual Rate (Hours/Year)
          <input onChange={this.updateRate} />
        </div>
        <div className="CalculatedOutput">
          <h3>Beep boop boop</h3>
          <CalculatedOutput parentState={this.state} workingDays={this.workingDays}/>
        </div>
      </div>
    );
  }
}

export default App;
